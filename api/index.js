export default {
  renderPage(host, url) {
    return new Promise((resolve, reject) => {
      const response = pages[url] || null;

      setTimeout(() => {
        resolve(response);
      }, 1000);
    });
  },
};

const pages = {
  "/": {
    head: {
      title: "首頁",
      meta: [
        {
          hid: "description",
          name: "description",
          content: "首頁",
        },
        {
          hid: "keywords",
          name: "keywords",
          content: "首頁",
        },
      ],
    },
    module: "single",
    components: [
      {
        name: "navbar-light",
        attrs: {
          logo: "https://apro-part01.cosmo-demo.com.tw/archive/images/logo_header/LOGO.png",
          links: [
            {
              text: "首頁",
              href: "/",
            },
            {
              text: "關於我們",
              href: "/about",
            },
          ],
        },
      },
      {
        name: "banner-with-image",
        attrs: {},
      },
    ],
  },
  "/about": {
    head: {
      title: "關於我們",
      meta: [
        {
          hid: "description",
          name: "description",
          content: "關於我們",
        },
        {
          hid: "keywords",
          name: "keywords",
          content: "關於我們",
        },
      ],
    },
    module: "single",
    components: [
      {
        name: "navbar-light",
        attrs: {
          logo: "https://apro-part01.cosmo-demo.com.tw/archive/images/logo_header/LOGO.png",
          links: [
            {
              text: "首頁",
              href: "/",
            },
            {
              text: "關於我們",
              href: "/about",
            },
          ],
        },
      },
      {
        name: "banner-basic",
        attrs: {
          title: {
            text: "關於我們",
            style: {},
          },
          description: {
            text: "Quickly design and customize responsive mobile-first sites with Bootstrap, the world’s most popular front-end open source toolkit, featuring Sass variables and mixins, responsive grid system, extensive prebuilt components, and powerful JavaScript plugins.",
            style: {},
          },
        },
      },
    ],
  },
};
